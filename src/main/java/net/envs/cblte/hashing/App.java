package net.envs.cblte.hashing;

import java.util.concurrent.Callable;
import org.apache.commons.codec.digest.DigestUtils;
import picocli.CommandLine;
import picocli.CommandLine.Parameters;

/**
 * Hello world!
 *
 */
@CommandLine.Command(name = "sha256hex", mixinStandardHelpOptions = true, version = "1.0", description = "Prints the sha256hex checksum of first parameter")
public class App implements Callable<Integer> {
    
    @Parameters(index = "0", description="The text input of which sha256 will be calculated.")
    private String input;
    
    public static void main(String[] args) {
        int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);
    }

    public static String sha2256hex(String data) {
        return DigestUtils.sha256Hex(data);
    }

    @Override
    public Integer call() throws Exception {
        System.out.printf("%s%n", sha2256hex(input));
        return 0;
    }
}
