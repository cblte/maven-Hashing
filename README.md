# maven-HelloWorld

Simple Hello World like hashing application in Java to test DroneCI with maven

It does not require any external depencies as everything is packed into a single jar file.

[![Build Status](https://drone.envs.net/api/badges/cblte/maven-Hashing/status.svg)](https://drone.envs.net/cblte/maven-Hashing)

## Compile, Run and Usage

$ git clone https://git.envs.net/cblte/maven-Hashing
$ cd maven-Hashing
$ mvn package
$ java -jar target/sha256hex.jar 123456

Output should be `8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92`

## Manual

Execute the jar file with no parameters or with the help parameter `-h`

```
$ java -jar target/sha256hex.jar -h
Usage: hashing [-hV] <input>
Prints the sha256hex checksum of first parameter
      <input>     The text input of which sha256 will be calculated.
  -h, --help      Show this help message and exit.
  -V, --version   Print version information and exit.

```

## Dependencies

Uses [picocli](https://picocli.info/) - a mighty tiny command line interface 